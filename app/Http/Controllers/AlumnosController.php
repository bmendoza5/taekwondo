<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Pago;
use App\Alumno;

class AlumnosController extends Controller
{
    public function index($Ncuenta){
    	$Adeudos = Pago::select('id', 'concepto','cantidad','abono','comentarios','fecha_cobro')->where('id_alumno','=',$Ncuenta)->where('estatus','=','1')->get();
    	
    	$Alumno = Alumno::select()->where('ncuenta','=', $Ncuenta)->get();

    	//dd($Adeudos);
    	return view('alumno/Index')->with('Alumno',$Alumno)
    							   ->with('Adeudos',$Adeudos);
    }

}
