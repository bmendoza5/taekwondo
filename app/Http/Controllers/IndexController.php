<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Routing\Controller;
use App\Alumno;
use App\Maestro;
use App\Evento;
use Carbon\Carbon;


use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){

        //Obtengo los eventos y los almaceno en una sesion para menor trafico de datos
        $date = Carbon::now();
        $Eventos = Evento::where('fechahora', '>=', $date)->get();
        session(['Eventos' => $Eventos]);

    	return view("Index");
    }

    public function acceder(){
        return view("Acceder");
    }

    public function login(Request $request){
        //Va a la BD y busca la existencia de Alumnos
        $ncuenta = $request->cuenta;
        $password= $request->contra;
        
        $validate = Alumno::select('ncuenta','nombres', 'apellidos','fnacimiento','id_maestro', 'id_grado', 'id_grupo')
            ->where('ncuenta', $ncuenta)
            ->where('contrasena', $password)->get()->first();


    	if($validate){

    		return redirect('alumnos/'.$validate['ncuenta'])->with('data',$validate);
    	}else{

            $validate = Maestro::select('ncuenta')
                                ->where('ncuenta', $ncuenta)
                                ->where('contrasena', $password)->get()->first();
            //dd($validate['ncuenta']);
                                
	        return redirect('/profesor/'.$validate['ncuenta'].'')->with('data',$validate);
    	}
    }

    public function eventos(){
        return view("Eventos");   
    }

    public function solicitud(){
        //$request = json_decode(file_get_contents('php://input')); 
        dd($request); 
        return 'aweob2.0';
    }

}













