<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Escuela;
use App\Grupo;
use App\Pago;
use App\Evento;
use Carbon\Carbon;
use App\Alumno;
use App\Maestro;
use App\Grado;
use App\Asistencia;

class MaestrosController extends Controller
{

    public function index($ncntMaestro){
    	//Obtengo las escuelas del maestro
    	$Escuelas = Escuela::select('id', 'nombre')
    				->where('id_maestro', $ncntMaestro)->get();

		//Obtengo los grupos asociados a esas escuelas
		$Esc = $Escuelas->all();
		$idGrupos = [];
    	foreach ($Esc as $item) {
    		array_push($idGrupos, $item['id']);
    	}
        
    	$GruposColection = [];
    	for ($i=0; $i < count($idGrupos) ; $i++) { 
    		array_push($GruposColection, Grupo::select('id', 'hrinicio', 'hrfin', 'id_escuela')
    								->where('id_escuela', $idGrupos[$i])->get());
    	}

		//Obtengo los adeudos del profesor
    	$Adeudos = Pago::select('cantidad', 'concepto', 'id_alumno', 'fecha_cobro')
    				->where('id_maestro', $ncntMaestro)
    				->where('estatus', 1)->get();

        //verify this lines because the events are not correct in this section, they only apear in eventos section
        //$date = Carbon::now();
        //$Eventos = Evento::select('nombres','apellidos', 'ntelefono','correo','id_grado')->where('fechahora', '>=', $date)->get();

        $personal_information_controller = Maestro::select('sbn','nombres','apellidos','ntelefono','correo','fnacimiento')->where('ncuenta', '=', $ncntMaestro)->get()->first()->toArray();

        $id_grado_belt = Maestro::select('id_grado')->where('ncuenta', '=', $ncntMaestro)->first();
        $belt_controller = Grado::select('clave','description')->where('id', '=', $id_grado_belt)->first()->toArray();

    	//Envio los resultados a la vista
    	return view('maestro/Index')->with('Escuelas', $Escuelas)
    								->with('Grupos', $GruposColection)
    								->with('Adeudos', $Adeudos)
                                    ->with('ncntMaestro', $ncntMaestro)
                                    ->with('personal_information_view', $personal_information_controller)
                                    ->with('belt_view', $belt_controller);
    }

    public function nuevoAlumno(Request $request){
        $date = Carbon::now();
        $Eventos = Evento::where('fechahora', '>=', $date)->get();

    	return view('maestro/registrarUsuario')->with('grupo',$request->grupo)
                                               ->with('escuela',$request->escuela)
                                               ->with('maestro', $request->maestro)
                                               ->with('Eventos', $Eventos);
    } 

    public function guardarAlumno(Request $request){
        $Date = Carbon::now();
        $year = $Date->format('Y');
        $numYear = str_split($year);
        $NalumnosRegister = 1 + Alumno::count();
        if($NalumnosRegister<100){
            $NalumnosRegister = '0'.$NalumnosRegister;
        }
        //numero de cuenta
        $ncuenta = $numYear[2].$numYear[3].'01'.$NalumnosRegister;

        $newStudent = new Alumno();
        $newStudent->ncuenta        = $ncuenta;
        $newStudent->contrasena     = $ncuenta;
        $newStudent->nombres        = $request->input('names');
        $newStudent->apellidos	    = $request->input('lastName');
        $newStudent->ocupacion      = $request->input('ocupation');
        $newStudent->edocivil       = $request->input('edoCivil');
        $newStudent->fnacimiento    = $request->input('born');
        $newStudent->num_grado      = $request->input('numgrado');
        $newStudent->tutor          = $request->input('tutor');
        $newStudent->contacto       = $request->input('cellphone');
        $newStudent->seguro         = $request->input('seguro');
        $newStudent->id_maestro     = $request->input('profesor');
        $newStudent->id_grado       = $request->input('id_grado');
        $newStudent->id_grupo	    = $request->input('grupo');

        $newStudent->save();

          return response()->json([
              'message' => $ncuenta
            ], 200);
    }

    public function Get_Attendance_list($idgroup){//$request->input('group')
        return Alumno::select('ncuenta','nombres','apellidos')->where('id_grupo','=',$idgroup)->get();
    }

    public function Save_attendance_list(Request $request){
        $complete_attendance_list = $request->all();
        
        foreach ($complete_attendance_list as $student) {
            $attendance = new Asistencia();       
            $attendance->estatus    = $student['estatus'];
            $attendance->id_alumno  = $student['id_alumno'];
            $attendance->id_grupo   = $student['id_grupo'];
            $attendance->save();
        }
        
        return response(200);
    }


}



/*********VER ESTO PARA CONTINUAR
// collection will be a collection of plain arrays, not Users models. 
$subset = $users->map(function ($user) { 
    return collect($user->toArray()) 
     ->only(['id', 'name', 'email']) 
     ->all(); 
}); */