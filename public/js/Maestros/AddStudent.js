
//All Jquery requerido 
$(document).ready(function(){
	 $('#datepicker').datepicker({
        uiLibrary: 'bootstrap4'
    });
});

//import Vue from 'vue';

//date picker con vue
//https://styde.net/agregando-un-datepicker-con-vue-js/


var vm_addStudent = new Vue({
	el: '#Register_new_student',
    data:  {
		validateAllRows: true,
      	registro:{
    		names: '',
			lastName: '',
			born: Date(Date.now()),
			ocupation: '',
			edoCivil: 'Soltero',
			numgrado: 'Grado',
			id_grado: 'Tipo',
			tutor: '',
			cellphone: '',
			payment: '',
			seguro: '',
			profesor: ProfesorPHP,
			grupo: GroupPHP
    	}
	},
	components:{
		vuejsDatepicker
	},
    methods:{
    	know_if_string_is_null: function(word){
    		if(word){
    			return true;
    		}else{
    			this.validateAllRows = false;
    		}
    	},
    	validateifContainLetters: function(cellphone){
    		if(isNaN(cellphone)){
    			this.validateAllRows = false;
    		}
    	},
    	validate11NumbersinSegure: function(numeroSeg){
    		if(numeroSeg.length != 11){
    			this.validateAllRows = false;
    		}
    		this.validateifContainLetters(numeroSeg);
		},
		validate_all_fields_from_form: function(){
			this.know_if_string_is_null(this.registro.names);
    		this.know_if_string_is_null(this.registro.lastName);
    		this.know_if_string_is_null(this.registro.ocupation);
    		this.know_if_string_is_null(this.registro.edoCivil);
    		this.know_if_string_is_null(this.registro.tutor);
    		this.validateifContainLetters(this.registro.cellphone);
    		this.validateifContainLetters(this.registro.payment);
    		if(this.registro.seguro){
    			this.validate11NumbersinSegure(this.registro.seguro);	
			}
		},
    	validateFields: function(){
    		this.validateAllRows = true;
			this.validate_all_fields_from_form();
			//obtener en formato YYYY-MM-03
			try{
				this.registro.born = (this.registro.born.toJSON().split('T'))[0];
			}catch(err){
				this.registro.born.toString();
			}

			if(this.validateAllRows == true){
				//the rows are valid and can sen to database		
				axios.post('/profesor/guardarAlumno', this.registro)
					.then(function (response) {
						if(response.status == 200){
							var New_ncuenta_registrated = JSON.parse(response.request.responseText);
							alert('se registró el usuario con numero de cuenta: '+ New_ncuenta_registrated.message);
						}
					})
					.catch(function (error) {
						alert('Lo sentimos, algo salio mal :(');
					});
    		}else{
    			//show where is the incorrect field in view
    			alert('Debes llenar todos los campos requeridos');
    		}
		}
	}
});



/*
//----> enviar con axios--- consultar: https://codeburst.io/embedding-php-forms-in-vue-js-b388f2d3c47

/  enviar los datos ---- guardar el contexto en variable
var self = this;
this.$http.get().then(function (respuesta) {
  self.lista = respuesta.body;
});*/

//ajax con jquery ---  https://styde.net/ajax-y-laravel-usando-jquery/









