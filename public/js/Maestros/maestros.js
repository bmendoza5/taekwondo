//**************************JQuery Requerido

//calendarios de profesores
$(document).ready(function(){
	$('#datetimepicker3').timepicker({
        uiLibrary: 'bootstrap4'
    });
    $('#datetimepicker4').timepicker({
        uiLibrary: 'bootstrap4'
    }); 
});


var grps = GroupsPHP;

function fillGroups(htmlElement, div, idToSend, idForGroup, divToPutnewText, idToViewGroupText){
	$(div).html("");
	$(grps[0]).each(function(i){
		if(this.id_escuela == htmlElement.value){
			if(this != null){
				$(this).each(function(j){
					var component = '<option class="dropdown-item" value="'+this.id+'" onclick="ObtainValueGroup(this,\''+idForGroup+'\', \''+idToViewGroupText+'\')" >'+this.hrinicio +' - '+this.hrfin+'</option>'
					$(idToSend).val(htmlElement.value);
					$(div).append(component);
				});
			}
		}else{
			$(idToViewGroupText).text('Grupo');
			var component = '<option class="dropdown-item" value="0">No existen grupos aún</option>'
			$(div).append(component);
			return false;
		}
	});
	
	changeViewsFromDropdown(divToPutnewText, htmlElement.text);
}

function ObtainValueGroup(htmlElement, idToSend, ButtonForShowText){
	$(idToSend).val(htmlElement.value);
	$(ButtonForShowText).text(htmlElement.text);
}

function changeViewsFromDropdown(divToPut, Text){
	$(divToPut).text( Text);
}



//VUE.JS Requerido
var EventBus = new Vue({});
window.selected_students_public = [];
window.group_selected = '';
window.school_selected = '';

Vue.component('modal', {
	created() {
		this.list_students = window.selected_students_public;
		this.school = window.school_selected;
		this.group = window.group_selected;
	},
	data(){
		return{
			list_students :'',
			group: '',
			school: '',
			attendance:[]
		}
	},
	template: '#modal-template',
	methods:{
		carge_attendance(){
			var complet_attendance_list = this.list_students.map(student =>{
				return {
					'id_alumno': student.ncuenta,
					'estatus' : this.attendance.includes(student.ncuenta),
					'id_grupo': ''
				}
			});
			vm_deshboard.save_attendance_list(complet_attendance_list);
		}
	}
});


var vm_deshboard = new Vue({
    el: '#section_groups_from_asistencias',
    data:  {
		Schools: SchoolsPHP,
		Groups: GroupsPHP,
		valuesAndKeysOfValues: [{hrinicio: 'Selecciona una escuela'}],
		guion : '',
		schoolSelected: 'Selecciona la escuela',
		groupSelected: 'Grupo',
		showModal: false,
		id_of_school_to_get_from_database: '',
		id_of_group_to_get_from_database: ''
	},
    methods:{
		//methods to take attendace
    	paintDropdownintTheView: function(objectOfSelectGroup){
    		this.valuesAndKeysOfValues = [];
    		for (var i = objectOfSelectGroup.length - 1; i >= 0; i--) {
    			this.valuesAndKeysOfValues.push(objectOfSelectGroup[i]);
    		}
    	},
    	obtainArrayofGroupsSelected: function(idEschool){
    		var returnTheSelectedGroups = new Object;

    		this.Groups.forEach(function(element,index){
    			if(element[index] != null){
    				if(element[index].id_escuela == idEschool){
    					returnTheSelectedGroups = element;
    				}
    			}
    		});
    		return returnTheSelectedGroups;
		},
		showSchoolSelectedinHTML: function(schoolSelectedByUser){
			this.schoolSelected = schoolSelectedByUser;
			window.school_selected = this.schoolSelected;
		},
		show_groups_Selectedin_HTML: function(selected_group_byUser_init,selected_group_byUser_end,idof_Group){
			this.id_of_group_to_get_from_database = idof_Group;
			this.groupSelected = selected_group_byUser_init + " - " + selected_group_byUser_end;
			window.group_selected = this.groupSelected;
		},
    	Fill_dropdownFrom_groups: function(idEschool, schoolselected){
			this.showSchoolSelectedinHTML(schoolselected);
			this.id_of_school_to_get_from_database = idEschool;

    		var ArrayofGroupsToSelectedSchool = this.obtainArrayofGroupsSelected(idEschool);
    		if($.isEmptyObject(ArrayofGroupsToSelectedSchool)){
				ArrayofGroupsToSelectedSchool = [{hrinicio: 'No existen grupos'}];
				this.groupSelected= 'Grupo';
				this.guion = '';
			}else{
				this.guion = ' - ';
			}
			this.paintDropdownintTheView(ArrayofGroupsToSelectedSchool);
		},
		get_students_from_selected_group(){
			let route = '/profesor/ListaAsistencia/' + this.id_of_group_to_get_from_database+'';
			if(this.groupSelected != "No existen grupos - undefined"){
				axios.get(route)
					.then(response =>{
						if(response.status == 200){
							window.selected_students_public = response.data;
							this.showModal = true;
						}
					}).catch(err =>{
						alert("Algo salió mal, vuelve a intentarlo!");;
					});
			}
			
		},
		button_check_atendence: function(){
		 	if(this.schoolSelected == 'Selecciona la escuela' || this.groupSelected =='Grupo' || this.groupSelected =='No existen grupos'){
				alert("Selecciona un grupo");
			}else{
				this.get_students_from_selected_group();
			 }
		},
		save_attendance_list(attendance){
			attendance.forEach(element => {
				element.id_grupo = this.id_of_group_to_get_from_database;
			});
			//****REMEMBER!!!
			//THE URL HAVEN'T CSRF VERIFICATION BECAUSE IS ON THE MIDLEWARE CHANGE THAT
			axios.post('/profesor/saveAttendance', attendance)
				.then((response) =>{
					if(response.status ==  200){
						alert('Guardado');
					}
				})
				.catch(err =>{
					alert('Algo ha salido mal, vuelve a intentarlo');
				});
			this.showModal = false;
		}
    }
});
























