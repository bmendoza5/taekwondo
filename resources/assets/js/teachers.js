require('./bootstrap');

window.Vue = require('vue');



Vue.component(
    'attendanceModal', 
    require('./components/teacher/attendanceModal.vue')
);
Vue.component('attendanceSection', require('./components/teacher/attendance.vue'));

const app = new Vue({
    el: '#app',
        
});
