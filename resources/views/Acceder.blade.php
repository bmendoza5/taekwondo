@extends('Layout')

@section('content')

	<link href="css/floating-labels.css" rel="stylesheet">

 	<!-- inicio form-->
 	<form method="POST" action="{{url('/acceder/validate')}}"  class="form-signin px-6 p-5">
    {{csrf_field()}}
      <div class="text-center mb-4">
        <img class="mb-6" src="{{ asset('images/logo.png') }}" alt="" width="120" height="95">

        <h1 class="h3 mb-3 font-weight-normal">Iniciar sesión</h1>
        <p>Asociaci&oacute; Mexicana Moo Duk Kwan - Colima.</p>
      </div>
    <!-- numero de cuenta-->
      <div class="form-label-group">
        <input type="text" id="ncuenta" class="form-control" placeholder="Numero de cuenta" required autofocus name="cuenta">
        <label for="ncuenta" class="py-1">Número de cuenta</label>
      </div>
    <!-- contraseña-->
      <div class="form-label-group mb-4">
        <input type="password" id="inputPassword" class="form-control" placeholder="Contraseña" required name="contra">
        <label for="inputPassword" class="py-1">Contraseña</label>
      </div>
      <a href="#" class="float-right">Olvidé mi contraseña</a>
    <button class="btn btn-primary btn-block mb-4" type="submit">Entrar</button>
      <br/>
  </form>
@endsection