@extends('Layout')

@section('content')


<div class="container-fluid pt-5">
	<div class="row">
		<div class="col-12 col-sm-4 col-md-3 col-lg-2 bg-secondary side-bar">
			<h4 class="text-white mt-4 mb-3">Próximos eventos</h4>
			<div class="list-group mb-4">
				@foreach ($Eventos as $evento)
				<a href="#" class="list-group-item list-group-item-action list-group-item-light">{{$evento['nombre']}}</a>
				@endforeach
			</div>
		</div>

		
		<div class="col-12 col-sm-8 col-md-9 col-lg-10 mb-6">
			<div class="container-fluid">
				<div class="row" id="Register_new_student">
					<form @submit.prevent="validateFields" action="{{url('/profesor/guardarAlumno')}}">
					{{csrf_field()}}
					<!--action('UsuariosController@vista'-->
						<!-- REGISTRAR DATOS DEL ALUMNO - INICIO -->
							<div class="col-12">
								<div class="card my-4">
									<div class="card-body">
										<h4 class="card-title">Datos del Alumno</h4>
										
										<div class="container">
										  	<div class="row">
											    <div class="col-sm">
											      <div class="form-group">
												    <label for="nameInput">Nombre(s):*</label>
												    <input 
														v-model="registro.names" 
														type="text" id="nameInput" placeholder="Texto"
														class="form-control"
													>
												  </div>
											    </div>
											    <div class="col-sm">
											      <div class="form-group">
												    <label for="LastnameInput">Apellidos:*</label>
												    <input type="text" class="form-control" id="LastnameInput" placeholder="Texto" v-model="registro.lastName">
												  </div>
											    </div>
											    <div class="col-sm">
											    	<div class="form-group">
													    <label for="datepicker">Fecha de Nacimiento:*</label>
														<vuejs-datepicker v-model="registro.born" input-class="form-control"></vuejs-datepicker>
												  	</div>
											    </div>
										  	</div>
										</div>
										<div class="container">
										  	<div class="row">
											    <div class="col-sm">
											      <div class="form-group">
												    <label for="ocupacion">Ocupaci&oacute;n:*</label>
												    <input type="text" v-model="registro.ocupation" class="form-control" id="ocupacion" placeholder="Texto">
												  </div>
											    </div>
											    <div class="col-sm">
											      <div class="form-group">
												    <label class="mr-sm-2" for="EdoCivil">Estado civil:</label>
												    <br>
													<select v-model="registro.edoCivil" class="number-pick custom-select mb-2 mr-sm-2 mb-sm-0" id="EdoCivil">
														<option selected>Soltero</option>
														<option>Casado(a)</option>
														<option>Viudo(a)</option>
														<option>Divorciado(a)</option>
														<option>Unión libre</option>
													</select>
												  </div>
											    </div>
											    <div class="col-sm">
											    </div>
										  	</div>
										</div>
										<div class="container">
										  	<div class="row">
											    <div class="col-sm">
											    	<label class="mr-sm-2" for="Gardo">Grado Actual:*</label>
											    	<br>
													<select v-model="registro.numgrado" class="number-pick custom-select mb-2 mr-sm-2 mb-sm-0" id="Gardo">
														<option selected>Grado</option>
														<option value="1">1°</option>
														<option value="2">2°</option>
														<option value="3">3°</option>
														<option value="4">4°</option>
														<option value="5">5°</option>
														<option value="6">6°</option>
														<option value="7">7°</option>
														<option value="8">8°</option>
														<option value="9">9°</option>
														<option value="10">10°</option>
														<option value="11">11°</option>
													</select>
													<select v-model="registro.id_grado" class="number-pick custom-select mb-2 mr-sm-2 mb-sm-0" id="Tipo">
														<option selected>Tipo</option>
														<option value="1">Kup</option>
														<option value="2">Poom</option>
														<option value="3">Dan</option>
													</select>
											    </div>
											    <div class="col-sm"></div>
											    <div class="col-sm"></div>
										  	</div>
										</div>
									</div>
								</div>
							</div>
						<!-- REGISTRAR DATOS DEL ALUMNO - FIN -->

						<!--REGISTRAR DATOS DEL TUTOR - INICIO -->
						<div class="col-12">
							<div class="card my-4">
								<div class="card-body">
									<h4 class="card-title">Datos de Contacto</h4>
									<div class="container">
									  	<div class="row">
										    <div class="col-sm">
										      <div class="form-group">
											    <label for="nombreTutor">Tutor:</label>
											    <input type="text" class="form-control" id="nombreTutor" placeholder="Nombre y Apellido" v-model="registro.tutor">
											  </div>
										    </div>
										    <div class="col-sm">
										      <div class="form-group">
											    <label for="celular">Contacto:*</label>
											    <input type="text" class="form-control" id="celular" placeholder="Celular" v-model="registro.cellphone">
											  </div>
										    </div>
										    <div class="col-sm">
										    	<div class="form-group">
												    <label for="mensualidad">Mensualidad:</label>
												    <input id="mensualidad" class="form-control" v-model="registro.payment"/>
												  </div>
										    </div>
									  	</div>
									</div>
									<div class="container">
									  	<div class="row">
										    <div class="col-sm">
										      <div class="form-group">
											    <label for="numseguro">Número de Seguro:</label>
											    <input type="text" class="form-control" id="numseguro" placeholder="Ingresar" v-model="registro.seguro">
											  </div>
										    </div>
										    <div class="col-sm">
										    </div>
										    <div class="col-sm">
										    </div>
									  	</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12">
							<p>** Campos obligatorios</p>
						</div>
						<!--REGISTRAR DATOS DEL TUTOR - FIN -->

						<div class="col-12">
							<button type="button" class="btn btn-outline-danger btn-sm float-right ml-3">Cancelar</button>
							<button type="submit" class="btn btn-outline-success btn-sm float-right">Registrar</button>
						</div>
					</form>	
				</div>
			</div>
		</div>
	</div>
</div>	


<!-- All JS REQUIRED-->
<script>
	//Send values of profesor and group to VueJs
	let GroupPHP = @json($grupo);
	let ProfesorPHP = @json($maestro);
</script>

<script type="text/javascript" src="{{ asset('js/Maestros/AddStudent.js') }}">	</script>


@endsection