@extends('Layout')

@section('content')

<div class="container-fluid pt-5">
  <div class="row">
    <div class="col-12 col-sm-4 col-md-3 col-lg-2 bg-secondary side-bar text-center">
      <img class="mt-4 mb-3" src="{{ asset('images/logo.png') }}" alt="{{$personal_information_view['nombres']}}" width="100" height="75">
      <h5 class="text-white mt-4 mb-3">Tus Datos</h5>
      <div class="list-group mb-4 ">
        <strong>
          <p>Nombre: {{$personal_information_view['nombres']}}{{$personal_information_view['apellidos']}} <br/>
          Cinta: {{$belt_view['description']}}<br/>
          Teléfono: {{$personal_information_view['ntelefono']}}<br/>
          Correo: {{$personal_information_view['correo']}}</p>
        </strong>
        <a href="#" class="list-group-action link-as-text">Editar</a>
      </div>
      <div class="list-group mb-4">
        <a href="#" class="list-group-action link-as-text"><i class="far fa-list-alt"></i> Reportes de Asistencias</a>
        <a href="#" class="list-group-action link-as-text"><i class="fas fa-chart-pie"></i> Estadísticas de Uso</a>
      </div>
    </div>

    <div class="col-12 col-sm-8 col-md-9 col-lg-10">
      <div class="container-fluid">
        <div class="row">
          <!-- MANEJO DE ASISTENCIAS - INICIO -->
          <div class="col-12 col-lg-6" id="asistencias">
            <div class="card my-4">
              <div class="card-body" id="section_groups_from_asistencias">
                <h5 class="card-title">Asistencia</h5>
                
                <div class="row">
                  <div class="col-12 col-md-9 mb-4">
                    <div class="drop-groups p-0">
                      <p>Escuelas:</p>
                      <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle selects" type="button" id="AsistenciaEscuela" value="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @{{schoolSelected}}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                          <option class="dropdown-item"
                              v-on:click="Fill_dropdownFrom_groups(escuela.id, escuela.nombre)" 
                              v-for="escuela in Schools">
                            @{{escuela.nombre}}
                          </option>
                            
                        </div>
                        <input type="hidden" id="escValueAddForAsistence" name="escuela" value="">
                      </div>
                    </div>
                  </div>
                  <div class="col-12 col-md-3 mb-4">
                    <div class="drop-groups p-0">
                      <p>Grupos:</p>
                      <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle selects" type="button" id="SeeValueFromGroup" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          @{{groupSelected}}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="grupoAsistencias" >
                          <option 
                            class="dropdown-item" 
                            v-for="grupo in valuesAndKeysOfValues" 
                            v-on:click="show_groups_Selectedin_HTML(grupo.hrinicio,grupo.hrfin,grupo.id)">
                              @{{grupo.hrinicio}} @{{guion}} @{{grupo.hrfin}}
                          </option>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <button type="button" class="btn btn-outline-success btn-sm float-right" v-on:click="button_check_atendence()">Tomar asistencia </button>
                <modal v-if="showModal" @close="showModal = false" @save_attendance="save_attendance_list()"></modal>
              </div>
            </div>
          </div>
          <!-- MANEJO DE ASISTENCIAS - FIN -->

          <!--MODAL PARA TOMAR ASISTENCIA - INICIO-->
          <script type="text/x-template" id="modal-template">
            <transition name="modal">
              <div class="modal-mask">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="" id="exampleModalLongTitle">Lista de Asistencia</h5>
                      <button type="button" class="close"  aria-label="Close"  @click="$emit('close')">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <form @submit.prevent="carge_attendance" action="{{url('/profesor/saveAttendance')}}">
                    {{csrf_field()}}
                      <div class="modal-body">
                        <p><strong>@{{school}} - Grupo: @{{group}}</strong></p><br>
                        <table class="table table-sm table-hover">
                          <thead>
                            <tr>
                              <th>N° Cuenta</th>
                              <th>Nombre</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr v-for="(student,index) in list_students" :key="`student-${index}`">
                              <th>@{{student.ncuenta}}</th>
                              <td>@{{student.nombres}} @{{student.apellidos}}</td>
                              <td>
                                <label class="switch">
                                <input type="checkbox" v-model="attendance" :value="student.ncuenta"><span class="slider round"></span>
                                </label>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <!--CAMBIAR ESTRUCTURA DE TABLA POR DIV'S-->
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-outline-primary btn-sm" >Registrar asistencias</button>
                          <button type="button" class="btn btn-outline-danger btn-sm"  @click="$emit('close')">Cancelar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </transition>
          </script>
          <!--MODAL PARA TOMAR ASISTENCIA - FIN-->

          <!-- REGISTRO DE ALUMNOS Y GRUPOS - INICIO -->
          <div class="col-12 col-lg-6">
            <div class="card my-4">
              <div class="card-body">
            <form method="POST" action="{{url('/profesor/nuevoAlumno/'.$ncntMaestro.'')}}">
                  {{csrf_field()}}
                <h5 class="card-title">Agregar un Alumno</h5>
                  <div class="row">
                    <div class="col-12 col-md-9 mb-4">
                      <div class="drop-groups p-0">
                        <p>Escuela:</p>
                        <div class="dropdown">
                          <button class="btn btn-secondary dropdown-toggle selects" type="button" id="AddAlumno" value="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Nombre de la Escuela
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            @foreach ($Escuelas as $escuela)
                            <option onclick="fillGroups(this,'#grupoAgregar', '#escValueAdd', '#grpValueAdd', '#AddAlumno', '#groups_for_add_alumno')"class="dropdown-item" value="{{$escuela['id']}}">{{$escuela['nombre']}}</option>
                              @endforeach
                          </div>
                          <input type="hidden" id="escValueAdd" name="escuela" value="">
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-md-3 mb-4">
                      <div class="drop-groups p-0">
                        <p>Grupo:</p>
                        <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle selects" type="button" id="groups_for_add_alumno" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Grupo
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="grupoAgregar">
                          <option class="dropdown-item">Selecciona una Escuela</option>
                        </div>
                        <input type="hidden" name="grupo" id="grpValueAdd" value="">
                      </div>
                      </div>
                    </div>
                  </div>

                  <input type="hidden" name="maestro" id="" value="{{$ncntMaestro}}">
                  <button type="button" class="btn btn-outline-primary btn-sm float-left" data-toggle="modal" data-target="#modalNuevoGrupo">Nuevo Grupo</button>
                  <button type="submit" class="btn btn-outline-success btn-sm float-right">Aceptar</button>
                </form>	
              </div>
            </div>
          </div>
          <!-- REGISTRO DE ALUMNOS Y GRUPOS - FIN -->

          <!-- MANEJO DE ADEUDOS - INICIO -->
          <div class="col-12">
            <div class="card my-4">
              <div class="card-body">
                <h5 class="card-title">Adeudos</h5>
                
                <div class="drop-groups p-0">
                  <div class="input-group mb-3">
                    <p class="mr-5">Buscar:</p>
                    <div class="input-group-append search float-right">
                      <input type="text" class="form-control border-right-0 rounded-right-0" placeholder="Nombre" aria-label="Recipient's username" aria-describedby="basic-addon2">
                      <button class="btn btn-outline-secondary mh-38" type="button"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>

                <div class="my-4">
                  <table class="table table-sm table-hover bordered">
                    <thead class="bg-secondary text-white">
                      <tr>
                        <th>N° Cuenta</th>
                        <th>Nombre</th>
                        <th>Concepto</th>
                        <th>Cantidad</th>
                        <th>Cobro</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($Adeudos as $adeudo)
                        <tr>
                              <th>{{ $adeudo['id_alumno'] }}</th>
                          <td>Negrito Azabache Tostado</td>
                          <th>{{ $adeudo['concepto'] }}</th>
                          <th>{{ $adeudo['cantidad'] }}</th>
                          <th>{{ $adeudo['fecha_cobro'] }}</th>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- MANEJO DE ADEUDOS - FIN -->

          <!-- MANEJO DE SOLICITUDES - INICIO -->
          <div class="col-lg-2"></div>
          <div class="col-12 col-sm-12 col-lg-8 mb-5">
            <div class="card my-4">
              <div class="card-body">
                <h5 class="card-title">Generar solicitudes</h5>
                
                <div class="row">
                  <div class="col-12 col-md-9 mb-4">
                    <div class="drop-groups p-0">
                      <p>Escuelas:</p>
                      <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle selects" type="button" id="EscuelasSolicitudes" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" value="">
                        Nombre de la Escuela
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          @foreach ($Escuelas as $escuela)
                            <option class="dropdown-item" 
                                onclick="fillGroups(this,'#grupoSolicitudes', '#escValueSolicitud', '#grpValueSolicitud', '#EscuelasSolicitudes', '#button_generate_solicitudes')"
                                value="{{$escuela['id']}}">{{$escuela['nombre']}}
                            </option>
                            @endforeach
                        </div>
                        <input type="hidden" id="escValueSolicitud" name="escuela" value="">
                      </div>
                    </div>
                  </div>

                  <div class="col-12 col-md-3 mb-4">
                    <div class="drop-groups p-0">
                      <p>Grupos:</p>
                      <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle selects" type="button" id="button_generate_solicitudes" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Grupo
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="grupoSolicitudes">
                          <button class="dropdown-item" value="">Selecciona una Escuela</button>
                        </div>
                        <input type="hidden" value="" id="grpValueSolicitud" name="grupo">
                      </div>
                    </div>
                  </div>
                </div>

                <button type="button" class="btn btn-outline-success btn-sm float-right" data-toggle="modal" data-target="#modalSolicitudes">Generar</button>
              </div>
            </div>
          </div>
          <div class="col-lg-2"></div>

          <!-- MANEJO DE SOLICITUDES - FIN -->

        </div>
      </div>
    </div>
  </div>
</div>

<!------------------------- MODALES --------------------------->


<!-- MODAL NUEVO GRUPO - INICIO -->
<div class="modal fade" id="modalNuevoGrupo" tabindex="-1" role="dialog" aria-labelledby="mdlNuevoGrupo" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="mdlNuevoGrupo">Nuevo Grupo</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
          <div class="col-4">
            <div class="costo-escuela">
                <!--seleccionar escuela-->
            <div class="dropdown">
              Escuela:
              <button class="btn btn-secondary dropdown-toggle selects" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Seleccionar
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach ($Escuelas as $escuela)
                <button onclick="fillGroups(this,'#grupoAgregar')" class="dropdown-item" value="{{$escuela['id']}}">{{$escuela['nombre']}}</button>
                  @endforeach
              </div>
            </div>	
            <br>
            <!--seleccionar costo-->
            <div class="form-group">
                <label for="formGroupExampleInput">Mensualidad:</label>
                <input type="text" class="form-control" id="mensualidad" placeholder="Costo Mensual">
              </div>
          </div>
          </div>
          <div class="col-8">
            <!-- selector de dias de la semana-->
          <div class="weekDays-selector">
            <strong> Dias de la semana:</strong>
            <br>
            <input type="checkbox" id="weekday-mon" class="weekday" />
            <label for="weekday-mon">Lu</label>
            <input type="checkbox" id="weekday-tue" class="weekday" />
            <label for="weekday-tue">Ma</label>
            <input type="checkbox" id="weekday-wed" class="weekday" />
            <label for="weekday-wed">Mi</label>
            <input type="checkbox" id="weekday-thu" class="weekday" />
            <label for="weekday-thu">Ju</label>
            <input type="checkbox" id="weekday-fri" class="weekday" />
            <label for="weekday-fri">Vi</label>
            <input type="checkbox" id="weekday-sat" class="weekday" />
            <label for="weekday-sat">Sa</label>
            <input type="checkbox" id="weekday-sun" class="weekday" />
            <label for="weekday-sun">Do</label>
          </div>
          </div>
      </div>
          
      
      <!--seleccionar hora inicio y fin-->
      <div class="container">
          <div class="row">
              <div class='col-sm-6'>
                Hora de inicio:
                  <div class="form-group">
                      <div class='input-group date'>
                          <input type="text" class="form-control" id='datetimepicker3'>
                      </div>
                  </div>
              </div>
              <div class='col-sm-6'>
                Hora de fin:
                  <div class="form-group">
                      <div class='input-group date' >
                        <input type="text" class="form-control" id='datetimepicker4'>
                      </div>
                  </div>
              </div>
          </div>
      </div>        

        <div class="modal-footer">
          <button type="button" class="btn btn-outline-primary btn-sm">Crear Grupo</button>
          <button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal">Cancelar</button>
      </div>
      </div>
      </div>
  </div>
</div>	
<!-- MODAL NUEVO GRUPO - FIN -->


<!-- MODAL GENERAR SOLICITUDES - INICIO -->
<div class="modal fade" id="modalSolicitudes" tabindex="-1" role="dialog" aria-labelledby="exampleModalSolicitudes" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalSolicitudes">Generar Solicitudes</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p><strong>Alumnos con derecho a aplicar</strong></p>

          <table class="table table-sm table-hover">
            <thead>
              <tr>
                <th>N° Cuenta</th>
                <th>Nombre</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>0123456789</th>
                <td>Negrito Azabache Tostado</td>
                <td>
                  <label class="switch">
                    <input type="checkbox"><span class="slider round"></span>
                  </label>
                </td>
              </tr>
              <tr>
                <th>0123456789</th>
                <td>Negrito Azabache Tostado</td>
                <td>
                  <label class="switch">
                    <input type="checkbox"><span class="slider round"></span>
                  </label>
                </td>
              </tr>
              <tr>
                <th>0123456789</th>
                <td>Negrito Azabache Tostado</td>
                <td>
                  <label class="switch">
                    <input type="checkbox"><span class="slider round"></span>
                  </label>
                </td>
              </tr>
              <tr>
                <th>0123456789</th>
                <td>Negrito Azabache Tostado</td>
                <td>
                  <label class="switch">
                    <input type="checkbox"><span class="slider round"></span>
                  </label>
                </td>
              </tr>
              <tr>
                <th>0123456789</th>
                <td>Negrito Azabache Tostado</td>
                <td>
                  <label class="switch">
                    <input type="checkbox"><span class="slider round"></span>
                  </label>
                </td>
              </tr>
            </tbody>
      </table>
      <br>
      <p><strong>Alumnos sin derecho</strong></p>

          <table class="table table-sm table-hover">
            <thead>
              <tr>
                <th>N° Cuenta</th>
                <th>Nombre</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>0123456789</th>
                <td>Negrito Azabache Tostado</td>
              </tr>
              <tr>
                <th>0123456789</th>
                <td>Negrito Azabache Tostado</td>
              </tr>
              <tr>
                <th>0123456789</th>
                <td>Negrito Azabache Tostado</td>
              </tr>
              <tr>
                <th>0123456789</th>
                <td>Negrito Azabache Tostado</td>
              </tr>
              <tr>
                <th>0123456789</th>
                <td>Negrito Azabache Tostado</td>
              </tr>
            </tbody>
      </table>

        <div class="modal-footer">
          <button type="button" class="btn btn-outline-primary btn-sm">Generar solicitudes</button>
          <button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal">Cancelar</button>
        </div>
        </div>
      </div>
  </div>
</div>
<!-- MODAL GENERAR SOLICITUDES - FIN -->

<!-- MODAL ASISTENCIA - INICIO -->

<!-- MODAL ASISTENCIA - FIN -->


<!-- LLAMAMOS A TODOS LOS ARCHIVOS VUE.js/JQuery USADOS EN ESTA PAGINA-->
<script type="text/javascript">//envio todas las variables necesarias al js
  var GroupsPHP = @json($Grupos);
  var SchoolsPHP = @json($Escuelas);
</script>

<script type="text/javascript" src="{{ asset('js/Maestros/maestros.js') }}">	</script>

<script type="text/javascript">
  function changevalue(id){
    $('#EscuelasSolicitudes').attr('value',id.value).text(id.textContent);
  }
</script>

@endsection