@extends('Layout')

@section('content')

<div class="container-fluid pt-5">
	<div class="row">
		<div class="col-12 col-sm-4 col-md-3 col-lg-2 bg-secondary side-bar">
			<h4 class="text-white mt-4 mb-3">Próximos eventos</h4>
			<div class="list-group mb-4">
				<a href="#" class="list-group-item list-group-item-action list-group-item-light">Evento 1 torneo</a>
				<a href="#" class="list-group-item list-group-item-action list-group-item-light">Evento 2 torneo</a>
				<a href="#" class="list-group-item list-group-item-action list-group-item-light">Evento 3 torneo</a>
			</div>
		</div>

		<div class="col-12 col-sm-8 col-md-9 col-lg-10">
			<div class="container-fluid">
				<div class="row">

					<!-- AVANCE PERSONALIZADO - INICIO -->
					<div class="col-12">
						<div class="card my-4">
							<div class="card-body">
								<h5 class="card-title">Avance</h5>

								<div class="my-4">
									aqui el avance
								</div>
							</div>
						</div>
					</div>
					<!-- AVANCE PERSONALIZADO - FIN -->

					

					<!-- MANEJO DE ADEUDOS - INICIO -->
					<div class="col-12">
						<div class="card my-4">
							<div class="card-body">
								<h5 class="card-title">Adeudos</h5>

								<div class="my-4">
									<table class="table table-sm table-hover bordered">
										<thead class="bg-secondary text-white">
											<tr>
												<th>Limite de pago</th>
												<th>Concepto</th>
												<th>Cantidad</th>
												<th>Abonos</th>
												<th>Adeudo total</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											@if ($Adeudos != null)
											    @foreach ($Adeudos as $adeudo)
												    <tr>
														<th>{{$adeudo['fecha_cobro']}}</th>
														<th>{{$adeudo['concepto']}}</th>
														<th>{{$adeudo['cantidad']}}</th>
														<th>{{$adeudo['abono']}}</th>
														<th>{{((int)$adeudo['cantidad'])-((int)$adeudo['abono'])}}</th>
														<th>
															<button type="button" class="btn-outline-primary btn-sm">Abonar</button> &nbsp;
															<button type="button" class="btn btn-outline-success btn-sm">Pagar</button>
														</th>
													</tr>
												@endforeach
											@else
											    <th> No existen Adeudos para este usuario</th>
											    <th></th><th></th><th></th><th></th>
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- MANEJO DE ADEUDOS - FIN -->

					<!-- REGISTRO A EVENTOS - INICIO -->
					<div class="col-lg-2"></div>
					<div class="col-12 col-sm-12 col-lg-8 mb-5">
						<div class="card my-4">
							<div class="card-body">
								<h5 class="card-title">Registro a Eventos</h5>
								
								<div class="my-4">
									<table class="table table-sm table-hover bordered">
										<thead class="bg-secondary text-white">
											<tr>
												<th>Evento</th>
												<th>Fecha</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											@if (session('Eventos') != null)
											    @foreach (session('Eventos') as $evento)
												    <tr>
														<th>{{$evento['nombre']}}</th>
														<td>{{$evento['fechahora']}}</td>
														<td>
															<button type="button" class="btn btn-outline-secondary btn-sm float-right" data-toggle="modal" data-target="#RegistrarModal">Registro</button>
														</td>
													</tr>
												@endforeach
											@else
											    <th> No existen eventos registrados</th>
											    <th></th><th></th><th></th><th></th>
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-2"></div>
					<!-- REGISTRO A EVENTOS - FIN -->

				</div>
			</div>
		</div>
	</div>
</div>


<!-------------------------SECCION MODALES-------------------------->

<!---MODAL REGISTRARSE EN EVENTO --- INICIO------>
<div class="modal fade" id="RegistrarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Requiere confirmación</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ¿Seguro que deseas incribirte a este evento?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button> &nbsp;
        <button type="button" class="btn btn-success">Inscribirme</button>
      </div>
    </div>
  </div>
</div>
<!---MODAL REGISTRARSE EN EVENTO --- INICIO------>




@endsection