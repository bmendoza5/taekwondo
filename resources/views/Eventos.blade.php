@extends('Layout')
@section('content')

<div class="container-fluid pt-5">
	<div class="row">
		<div class="col-12 col-sm-8 col-md-9 col-lg-10">
			<div class="container-fluid">
				<div class="row">
					<!-- LISTADO DE EVENTOS - INICIO -->
					<div class="col-12">
						<div class="card my-4">
							<div class="card-body">
								<h5 class="card-title">Eventos</h5>
								<div class="drop-groups p-0">
									<div class="input-group mb-3">
										<p class="mr-5">Buscar:</p>
										<div class="input-group-append search float-right">
											<input type="text" class="form-control border-right-0 rounded-right-0" placeholder="Nombre" aria-label="Recipient's username" aria-describedby="basic-addon2">
											<button class="btn btn-outline-secondary mh-38" type="button"><i class="fas fa-search"></i></button>
										</div>
									</div>
								</div>

								<div class="my-4">
									<div id="accordion">
                    @foreach(session('Eventos') as $evento)
                      <div class="card">
                        <div class="card-header card-color" >
                          <h5 class="mb-0">
                            <button class="btn btn-link link" data-toggle="collapse" data-target="#card{{$evento['id']}}" aria-expanded="" aria-controls="collapseOne">
                              <h5>Evento: {{$evento['nombre']}}</h5>
                            </button>
                          </h5>
                        </div>
                        <div id="card{{$evento['id']}}" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                          <div class="card-body">
                              <div class="row">
                                <div class="col-12 col-md-2 mb-4">
                                  <div class="drop-groups p-0">
                                    <address>
                                      <strong>Direcci&oacute;n: </strong><br>
                                      {{$evento['direccion']}}<br>
                                    </address>
                                    <button type="button" class="btn btn-outline-success btn-sm float-left">Ver Mapa</button>
                                  </div>
                                </div>
                                <div class="col-12 col-md-1 mb-4">
                                </div>
                                <div class="col-12 col-md-2 mb-4">
                                  <div class="drop-groups p-0">
                                    <strong>Fecha y Hora:</strong><br>
                                    {{$evento['fechahora']}}<br>
                                  </div>
                                </div>
                                <div class="col-12 col-md-2 mb-4">
                                  <div class="drop-groups p-0">
                                    <br>
                                    <button type="button" class="btn btn-outline-info btn-sm" data-toggle="modal" data-target="#modalEventos">Solicitar mas informacion</button><br> <br>
                                  </div>
                                </div>
                              </div>
                              <button type="button" class="btn btn-outline-primary btn-sm float-right">Registrarse</button><br>
                          </div>
                        </div>
                      </div>
                    @endforeach
  								</div>
  							</div>
						</div>
					</div>
				</div>
        <!-- LISTADO DE EVENTOS - FIN -->
			</div>
		</div>
	</div>
</div>	

<!---- SECCIÓN MODALES-------------------------------------------------------------->
<!-- MODAL ASISTENCIA - INICIO -->
<div class="modal fade" id="modalEventos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Solicitar información</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <form method="post" v-on:submit="GuardarSolicitud">
      {{csrf_field()}}

        <div class="modal-body" id="solicitud">
          
          <div class="form-group">
            <label for="">Asunto:</label>
            <input type="text" class="form-control" v-model="asunto" name="asunto">
          </div>
          <div class="form-group">
            <label for="">Remitente: *</label>
            <input type="text" class="form-control" placeholder="Nombre y apellido" id="" v-model="remitente">
          </div>
          <div class="form-group">
            <label for="">Correo: * </label>
            <input type="text" class="form-control" placeholder="Aquí recibirá su respuesta" id="" v-model="correo">
          </div>
          <div class="form-group">
            <label for="">Comentario o pregunta: *</label>
            <textarea class="form-control" id="" placeholder="Compartenos tu pregunta" rows="3" v-model="pregunta"></textarea>
          </div>
          <br>
          <p>* Campos obligatorios</p>

          <div class="modal-footer">
            <button type="submit" class="btn btn-outline-primary btn-sm">Enviar solicitud</button>
            <button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal">Cancelar</button>
          </div>
        </div>
      </form>

      </div>
  </div>
</div>
<!-- MODAL ASISTENCIA - FIN -->

<script type="text/javascript" src="{{ asset('js/index.js') }}"> </script>
@endsection