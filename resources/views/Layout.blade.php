<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>AMMDK - Colima</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
     <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/principal.css')}}" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!--VUE.JS-->
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js"></script>
    <!-- SEND AJAX WITH VUE.JS--> 
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <!-- DATE PICKER WHIT VUE-JS-->
    <script src="https://unpkg.com/vuejs-datepicker"></script>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
  <script src="https://cdn.jsdelivr.net/vue.resource/1.3.1/vue-resource.min.js"></script>

  </head>

  <body>
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark py-1">
        <div class="hom">
          <a class="navbar-brand" href="/index"><i class="fas fa-home"></i></a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">&nbsp;&nbsp;<i class="fas fa-video"></i> Formas</a>
            </li>
            &nbsp;
            <li class="nav-item active">
              <a class="nav-link" href="/eventos"><i class="far fa-newspaper"></i> Eventos <span class="sr-only">(current)</span></a>
            </li>
            &nbsp;
            <li class="nav-item active">
              <a class="nav-link" href="#"><i class="far fa-envelope"></i> Contacto <span class="sr-only">(current)</span></a>
            </li>
          </ul>
          <div class="logo nav-link">
            <h5>¡Asoc. Méx. MDK - Colima!</h5>
          </div>
          
          <div class="form-inline mt-2 mt-md-0">
            <div>
              <a class="btn btn-light btn-sm" href="/acceder">Iniciar Sesión</a>  
            </div>
          </div>
          <div class="form-inline mt-2 mt-md-0" style="display: none;">
            <div class="hom">
              <a href="#" class="link"><i class="fas fa-user"></i> Bienvenido &nbsp;</a>
            </div>&nbsp;&nbsp;
            <a href="#" class="link"><i class="fas fa-user"></i> Salir &nbsp;</a>
          </div>
        </div>
      </nav>
    </header>

    
    <main role="main">
    	
      @yield('content')

    </main>

		<!-- FOOTER -->
		<div class="pie">
		  <p class="pl-4 p-footer">&copy; Asociación Mexicana MDK - Colima</p>
      <p class="float-right"><a href="#"><i class="fas fa-angle-up chevron"></i></a></p>
		</div>
    
    
  </body>
</html>
