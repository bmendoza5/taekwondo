<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     * Archivo generado automaticamente, si no es necesario, eliminar
     */
    public function testBasicTest()
    {
        $response = $this->get('/index');
        $response->assertStatus(200);
    }
}
