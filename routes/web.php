<?php


//***** vistas de index
Route::get('/index', 'IndexController@index');

Route::get('/acceder', 'IndexController@acceder');

Route::post('/acceder/validate', 'IndexController@login');

Route::get('/eventos', 'IndexController@eventos');

Route::post('/eventos/solicitud', 'IndexController@solicitud');


//**** Vistas seccion profesor
Route::get('/profesor/{id}','MaestrosController@index');

Route::post('/profesor/nuevoAlumno/{id}', 'MaestrosController@nuevoAlumno');

Route::post('/profesor/guardarAlumno', 'MaestrosController@guardarAlumno');

Route::get('/profesor/ListaAsistencia/{idgroup}', 'MaestrosController@Get_Attendance_list');

Route::post('/profesor/saveAttendance', 'MaestrosController@Save_attendance_list');


//***** vistas de control de alumno *** MAL
/*Route::get('/alumnos', function () {
    return view("Layout");
});*/

//Route::get('/alumnos/{id}', 'AlumnosController@index');//->where('id', '[0-9]+');

Route::get('/alumnos/{id}', 'AlumnosController@index');






