<?php

use Illuminate\Database\Seeder;
use App\Evento;

class EventosSeeder extends Seeder
{
    
    public function run()
    {
        factory(App\Evento::class,5)->create();
    }
}
