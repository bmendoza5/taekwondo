<?php

use Illuminate\Database\Seeder;
use App\Grupo;

class GruposSeeder extends Seeder
{
    
    public function run()
    {
    	$idEscuela = DB::table('escuelas')->select('id')->get()->first();

        factory(App\Grupo::class,7)->create([
        	'id_escuela' => $idEscuela->id
        ]);

        factory(App\Grupo::class,7)->create([
        	'id_escuela' => 3
        ]);

        factory(App\Grupo::class,7)->create([
        	'id_escuela' => 5
        ]);
    }
}
