<?php

use Illuminate\Database\Seeder;

class PagosSeeder extends Seeder
{
    
    public function run()
    {
        DB::table('pagos')->insert([
            'concepto' => 'Debe petos',
	        'cantidad' => '1000',
	        'estatus' => 1,
	        'abono' => 00,
	        'comentarios' => 'No quiere pagar',
	        'fecha_cobro' => '2019/1/12',
	        'id_maestro' => '123456789',
        	'id_alumno' => '987654321',
        ]);

        DB::table('pagos')->insert([
            'concepto' => 'Debe examen',
	        'cantidad' => '5000',
	        'estatus' => 1,
	        'abono' => 00,
	        'comentarios' => 'No quiere pagar',
	        'fecha_cobro' => '2019/2/12',
	        'id_maestro' => '123456789',
        	'id_alumno' => '987654321',
        ]);
    }
}
