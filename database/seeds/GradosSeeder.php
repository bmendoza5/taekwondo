<?php

use Illuminate\Database\Seeder;
//use Illuminate\Support\Facades\DB;

class GradosSeeder extends Seeder
{
    
    public function run()
    {
        DB::table('grados')->insert([
            ['id' => 11, 'clave' => '-', 'description' => 'Blanca principiante'],
            ['id' => 10, 'clave' => '10', 'description' => 'Blanca Avanzada'],
            ['id' => 9, 'clave' => '9', 'description' => 'Amarilla'],
            ['id' => 8, 'clave' => '8', 'description' => 'Amarilla Avanzada'],
            ['id' => 7, 'clave' => '7', 'description' => 'Verde Avanzada'],
            ['id' => 6, 'clave' => '6', 'description' => 'Verde'],
            ['id' => 5, 'clave' => '5', 'description' => 'Azul'],
            ['id' => 4, 'clave' => '4', 'description' => 'Azul Avanzada'],
            ['id' => 3, 'clave' => '3', 'description' => 'Marron'],
            ['id' => 2, 'clave' => '2', 'description' => 'Marron Avanzada'],
            ['id' => 1, 'clave' => '1', 'description' => 'Roja'],
            ['id' => 0, 'clave' => '1', 'description' => 'Roja Yebidan'],
            //blackbelt
            ['id' => 12, 'clave' => '1', 'description' => 'Poom'],
            ['id' => 13, 'clave' => '2', 'description' => 'Poom'],
            ['id' => 14, 'clave' => '3', 'description' => 'Poom'],
            ['id' => 15, 'clave' => '4', 'description' => 'Poom'],
            //blackbelt - +15 years old
            ['id' => 16, 'clave' => '1', 'description' => 'Dan'],
            ['id' => 17, 'clave' => '2', 'description' => 'Dan'],
            ['id' => 18, 'clave' => '3', 'description' => 'Dan'],
            ['id' => 19, 'clave' => '4', 'description' => 'Dan'],
            ['id' => 20, 'clave' => '5', 'description' => 'Dan'],
            ['id' => 21, 'clave' => '6', 'description' => 'Dan'],
            ['id' => 22, 'clave' => '7', 'description' => 'Dan'],
            ['id' => 23, 'clave' => '8', 'description' => 'Dan'],
            ['id' => 24, 'clave' => '9', 'description' => 'Dan'],
        ]);
    }
}
