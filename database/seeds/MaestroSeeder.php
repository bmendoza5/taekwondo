<?php

use Illuminate\Database\Seeder;
use App\Maestro;

class MaestroSeeder extends Seeder
{
    
    public function run()
    {
    	DB::table('maestros')->insert([
            'ncuenta' => '123456789',
	        'contrasena' => '123456789',
	        'sbn' => str_random(4),
	        'nombres' => str_random(10),
	        'apellidos' => str_random(7).' '.str_random(7),
	        'ntelefono' => '3125951050',
	        'correo' => 'asd@gmail.com',
	        'faltas' => 3,
        	'fnacimiento' => '1996-12-12',
        	'id_grado' => 3
        ]);

        factory(App\Maestro::class,10)->create([
        	'faltas' => 3,
        	'fnacimiento' => '1996-12-12',
        	'id_grado' => 3
        ]);
    }
}
