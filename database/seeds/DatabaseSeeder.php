<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    
    
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(GradosSeeder::class);
        $this->call(MaestroSeeder::class);
        $this->call(EscuelasSeeder::class);
        $this->call(GruposSeeder::class);
        $this->call(AlumnosSeeder::class);
        $this->call(EventosSeeder::class);
        $this->call(PagosSeeder::class);
    }
}
