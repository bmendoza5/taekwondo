<?php

use Illuminate\Database\Seeder;
use App\Alumno;

class AlumnosSeeder extends Seeder
{
    
    public function run()
    {
        $idGrado = DB::table('grados')->select('id')->get()->first();
        $idGrupo = DB::table('escuelas')->select('id')->get()->first();
        $idMaestro = DB::table('maestros')->select('ncuenta')->get()->first();

        factory(App\Alumno::class,30)->create([
            'num_grado' => 5,
        	'id_maestro' => $idMaestro->ncuenta,
        	'id_grado' => $idGrado->id,
        	'id_grupo' => $idGrupo->id
        ]);

        factory(App\Alumno::class,30)->create([
            'num_grado' => 2,
        	'id_maestro' => $idMaestro->ncuenta,
        	'id_grado' => 3,
        	'id_grupo' => 6
        ]);

        factory(App\Alumno::class,30)->create([
            'num_grado' => 10,
        	'id_maestro' => $idMaestro->ncuenta,
        	'id_grado' => 1,
        	'id_grupo' => 10
        ]);
        
        factory(App\Alumno::class,5)->create([
            'num_grado' => 5,
        	'id_maestro' => '123456789',
        	'id_grado' => 1,
        	'id_grupo' => 7
        ]);

        DB::table('alumnos')->insert([
            'ncuenta' => '987654321',
            'contrasena' => '987654321',
            'nombres' => 'Anastacio',
            'apellidos' => 'Elva Ginon',
            'ocupacion' => 'Estudiante',
            'edocivil' => 'Soltero',
            'fnacimiento' => '2012-12-12',
            'id_maestro' => $idMaestro->ncuenta,
            'id_grado' => 10,
            'id_grupo' => 10
        ]);

    }
}
