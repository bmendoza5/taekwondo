<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    

    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('nombre');
            $table->string('direccion');
            $table->string('sinodal');
            $table->datetime('fechahora');
            
            $table->timestamps();
        });
    }

    


    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
