<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagosTable extends Migration
{
    
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('concepto',100);
            $table->integer('cantidad');
            $table->tinyInteger('estatus');
            $table->integer('abono');
            $table->string('comentarios');
            $table->date('fecha_cobro');
            $table->date('fecha_pago')->nullable();


            $table->timestamps();
        });
    }


    /*** Reverse the migrations.*/
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
