<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToTables extends Migration
{
    /* Si no es marcado error - restringir el tamaño de caracteres en todos los trings*/
    public function up()
    {
        Schema::table('pagos', function(Blueprint $table){
            $table->string('id_maestro',10);
            $table->foreign('id_maestro')->references('ncuenta')->on('maestros');

            $table->string('id_alumno',10);
            $table->foreign('id_alumno')->references('ncuenta')->on('alumnos');
        });

        Schema::table('escuelas', function(Blueprint $table){
            $table->string('id_maestro');
            $table->foreign('id_maestro')->references('ncuenta')->on('maestros');
        });

        Schema::table('patrocinadores', function(Blueprint $table){
            $table->string('id_alumno');
            $table->foreign('id_alumno')->references('ncuenta')->on('alumnos');
        });

        Schema::table('avisos', function(Blueprint $table){
            $table->string('id_creador');
            $table->foreign('id_creador')->references('ncuenta')->on('maestros');
        });

        Schema::table('eventos', function(Blueprint $table){
            $table->integer('id_escuela')->unsigned();
            $table->foreign('id_escuela')->references('id')->on('escuelas');
        });

        Schema::table('grupos', function(Blueprint $table){
            $table->integer('id_escuela')->unsigned();
            $table->foreign('id_escuela')->references('id')->on('escuelas');
        });

        Schema::table('asistencias', function(Blueprint $table){
            $table->string('id_alumno');
            $table->foreign('id_alumno')->references('ncuenta')->on('alumnos');

            $table->integer('id_grupo')->unsigned();
            $table->foreign('id_grupo')->references('id')->on('grupos');
        });

        Schema::table('maestros', function(Blueprint $table){
            $table->smallInteger('id_grado');
            $table->foreign('id_grado')->references('id')->on('grados');
        });

        Schema::table('alumnos', function(Blueprint $table){
            $table->string('id_maestro',10);
            $table->foreign('id_maestro')->references('ncuenta')->on('maestros');

            $table->smallInteger('id_grado');
            $table->foreign('id_grado')->references('id')->on('grados');

            $table->integer('id_grupo')->unsigned();
            $table->foreign('id_grupo')->references('id')->on('grupos');
        });

    }

    
    public function down()
    {
        Schema::table('pagos',function(Blueprint $table){
            $table->dropForeign('id_maestro_alumno_foreig');
        });

        Schema::table('escuelas', function(Blueprint $table){
            $table->dropForeign('id_maestro_foreig');
        });

        Schema::table('patrocinadores', function(Blueprint $table){
            $table->dropForeign('id_patrocinadores_foreig');
        });

        Schema::table('avisos', function(Blueprint $table){
            $table->dropForeign('id_creador_foreig');
        });

        Schema::table('eventos', function(Blueprint $table){
            $table->dropForeign('id_escuela_foreig');
        });

        Schema::table('grupos', function(Blueprint $table){
            $table->dropForeign('id_escuela_foreig');
        });

        Schema::table('asistencias', function(Blueprint $table){
            $table->dropForeign('id_grupo_alumno_maestro_foreig');
        });

        Schema::table('maestros', function(Blueprint $table){
            $table->dropForeign('id_grado_foreig');
        });

        Schema::table('alumnos',function(Blueprint $table){
            $table->dropForeign('id_maestro_foreing');
        });
    }
}
