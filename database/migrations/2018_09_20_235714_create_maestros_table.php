<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaestrosTable extends Migration
{
    

    public function up()
    {
        Schema::create('maestros', function (Blueprint $table) {
            $table->string('ncuenta',10)->primary();
            $table->string('contrasena',200);
            
            $table->string('sbn');
            $table->string('nombres',100);
            $table->string('apellidos',100);
            $table->string('ntelefono',15);
            $table->string('correo',60)->unique();
            $table->date('fnacimiento');
            $table->integer('faltas');

            $table->timestamps();
        });
    }

    

    public function down()
    {
        Schema::dropIfExists('maestros');
    }
}
