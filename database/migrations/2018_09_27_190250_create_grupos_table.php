<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGruposTable extends Migration
{
   

    public function up()
    {
        Schema::create('grupos', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('hrinicio',30);
            $table->string('hrfin',30);
            $table->string('dias');
            $table->timestamps();
        });
    }

    

    public function down()
    {
        Schema::dropIfExists('grupos');
    }
}
