<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatrocinadoresTable extends Migration
{
    
    public function up()
    {
        Schema::create('patrocinadores', function (Blueprint $table) {
            $table->increments('id');

            $table->string('eslogan',150);
            $table->string('nombre',100);
            $table->string('telefono',15);
            $table->string('correo',100)->unique();
            $table->string('direccion',100);


            $table->timestamps();
        });
    }



    public function down()
    {
        Schema::dropIfExists('patrocinadores');
    }
}
