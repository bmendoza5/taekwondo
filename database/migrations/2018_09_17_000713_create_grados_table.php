<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradosTable extends Migration
{
    
    /*
    */
    public function up()
    {
        Schema::create('grados', function (Blueprint $table) {
            $table->smallInteger('id')->primary();
            $table->string('clave');
            $table->string('description');
        });
    }

    


    public function down()
    {
        Schema::dropIfExists('grados');
    }
}
