<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlumnosTable extends Migration
{
    
    public function up()
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->string('ncuenta',10)->primary();
            $table->string('contrasena',25);
            
            $table->string('nombres',100);
            $table->string('apellidos',100);
            $table->string('ocupacion',30);
            $table->string('edocivil',30);
            $table->date('fnacimiento');
            $table->tinyInteger('num_grado');

            $table->string('tutor',100);
            $table->string('contacto',15);
            $table->string('seguro', 11)->nullable();

            $table->timestamps();
        });

    }

    


    public function down()
    {
        Schema::dropIfExists('alumnos');
    }
}
