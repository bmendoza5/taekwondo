<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEscuelasTable extends Migration
{
    
    public function up()
    {
        Schema::create('escuelas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre',70);
            $table->string('eslogan',150);
            $table->string('numescuela',50);
            $table->string('coordenadas',100);
            $table->string('direccion',150);
            
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('escuelas');
    }
}
