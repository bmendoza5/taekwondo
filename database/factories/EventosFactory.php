<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Evento::class, function (Faker $faker) {

    return [
        'nombre' => $faker->sentence(2),
        'direccion' => $faker->sentence(2),
        'sinodal' => $faker->sentence(1),
        'fechahora' => Carbon::now()->subDays(rand(0, 7))->format('Y-m-d'),
        'id_escuela' => 1
    ];
});
