<?php

use Faker\Generator as Faker;

$factory->define(App\Alumno::class, function (Faker $faker) {
    return [
    	'ncuenta' => str_random(10),
    	'contrasena' => str_random(10),
    	'nombres' => $faker->sentence(2),
    	'apellidos' => $faker->sentence(2),
    	'ocupacion' => $faker->sentence(1),
    	'edocivil' => $faker->sentence(1),
    	'fnacimiento' => '2012-12-12',
    	'tutor'=> $faker->sentence(2),
    	'contacto' => str_random(10),
        'seguro' => str_random(11),
    ];
});
