<?php

use Faker\Generator as Faker;

$factory->define(App\Grupo::class, function (Faker $faker) {
    return [
        'hrinicio' => mt_rand(4, 10),
        'hrfin' => mt_rand(4, 10),
        'dias' => 'L, M, V'
    ];
});
