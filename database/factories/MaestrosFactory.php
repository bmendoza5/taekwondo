<?php

use Faker\Generator as Faker;

$factory->define(App\Maestro::class, function (Faker $faker) {
    return [
        'ncuenta' => str_random(10),
        'contrasena' => str_random(10),
        'sbn' => str_random(4),
        'nombres' => $faker->sentence(2),
        'apellidos' => $faker->sentence(2),
        'ntelefono' => '3125951050',
        'correo' => $faker->email
    ];
});